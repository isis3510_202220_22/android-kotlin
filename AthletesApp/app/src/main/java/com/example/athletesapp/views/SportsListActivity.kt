package com.example.athletesapp.views

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.athletesapp.R
import com.example.athletesapp.models.CountDocument
import com.example.athletesapp.models.Sport
import com.example.athletesapp.utils.ConnectionProvider
import com.example.athletesapp.utils.MyCache
import com.example.athletesapp.viewmodels.LookingForViewModel
import com.example.athletesapp.viewmodels.SportViewModel
import com.example.athletesapp.viewmodels.UserViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.URL

class SportsListActivity : AppCompatActivity() {
    private var parentLinearLayout: LinearLayout? = null
    private var parentFrameLayout: FrameLayout? = null
    private var backButton: ImageButton? = null
    private lateinit var connectionProvider: ConnectionProvider
    private val lookingForViewModel: LookingForViewModel by viewModels()
    private var countDocument: CountDocument? = null
    private var counts: ArrayList<TextView> = arrayListOf()
    private val sportViewModel: SportViewModel by viewModels()
    private var sports: ArrayList<Sport> = arrayListOf()
    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sports_list)
        removePreferences()
        userViewModel = UserViewModel(this.application)
        userViewModel.saveCurrentUser(intent.getStringExtra("email"))

        connectionProvider = ConnectionProvider(this)
        parentLinearLayout = findViewById(R.id.linearLayout)

        sports = sportViewModel.getSports()

        for (sport in sports) {
            if (sport.name == intent.getStringExtra("favSport")) {
                sports.remove(sport)
                sports.add(0, sport)
                break
            }
        }

        loadSports()
        loadBackButton()
        initLookingForViewModel()
        lookingForViewModel.getCountDocument()
    }

    private fun removePreferences() {
        val sharedPref = this.getSharedPreferences("progress_tracker",Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            clear()
            apply()
        }
    }

    private fun initLookingForViewModel() {
        lookingForViewModel.getCountDocumentLiveData.observe(this) {
            onGetCountDocument(it)
        }
    }

    private fun getImageOnline(i: Int, icon: ImageView) {
        GlobalScope.launch(Dispatchers.IO) {
            try{
                val newurl: URL = if (i == 0) {
                    URL(sports[i].favIconFile)
                } else {
                    URL(sports[i].defaultIconFile)
                }

                val mIconVal = BitmapFactory.decodeStream(newurl.openConnection().getInputStream())

                runOnUiThread {
                    if (mIconVal != null) {
                        icon.setImageBitmap(mIconVal)
                        if (i == 0) {
                            MyCache.instance.saveBitmapToCache(
                                sports[i].name.lowercase() + "yellow",
                                mIconVal
                            )
                        } else {
                            MyCache.instance.saveBitmapToCache(
                                sports[i].name.lowercase() + "default",
                                mIconVal
                            )
                        }
                    }
                }
            }
            catch (e: Exception){

            }
        }
    }

    private fun loadSports() {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var rowView: View?
        var name: TextView?
        var icon: ImageView?
        var container: ConstraintLayout?
        val userEmail: String? = intent.getStringExtra("email")

        for (i in sports.indices.reversed()) {
            rowView = inflater.inflate(R.layout.sport_card, null)

            rowView.setOnClickListener {
                val intent = Intent(this, UsersListActivity::class.java)
                intent.putExtra("sport", sports[i].name)
                intent.putExtra("email", userEmail)
                startActivity(intent)
            }

            parentLinearLayout!!.addView(rowView, 0)

            name = findViewById(R.id.textView2)
            name.text = sports[i].name

            icon = findViewById(R.id.imageView10)

            val image: Bitmap? = if (i == 0) {
                MyCache.instance.retrieveBitmapFromCache(sports[i].name.lowercase() + "yellow")
            } else {
                MyCache.instance.retrieveBitmapFromCache(sports[i].name.lowercase() + "default")
            }
            if(image!=null){
                icon.setImageBitmap(image)
            }
            else
            {
                if(connectionProvider.connectivityAvailable())
                {
                    if (i == 0) {
                        getImageOnline(i, icon)
                    }
                    else {
                        getImageOnline(i, icon)
                    }
                }
                else
                {
                    icon.setImageResource(R.mipmap.ic_default_sports)
                }
            }

            val cantUsers: TextView? = findViewById(R.id.textView8)
            if (cantUsers != null) {
                counts.add(cantUsers)
            }

            if (i == 0) {
                container = findViewById(R.id.constraintLayout)
                container.setBackgroundResource(R.drawable.layout_border)
            }
        }

        if(!connectionProvider.connectivityAvailable()) {
            val snackbar: Snackbar = Snackbar
                .make(window.decorView.findViewById(android.R.id.content), "There was a problem connecting you to the internet. Some features might not be available", Snackbar.LENGTH_LONG)
            snackbar.show()
        }
    }

    fun onGetCountDocument(it: CountDocument) {
        countDocument = it

        for (i in sports.indices.reversed()) {
            val cantUsers: TextView = counts[sports.size-i-1]

            when(sports[i].name)
            {
                "Football" -> cantUsers.text = countDocument?.football.toString() + " users"
                "Basketball" -> cantUsers.text = countDocument?.basketball.toString() + " users"
                "Tennis" -> cantUsers.text = countDocument?.tennis.toString() + " users"
                "Volleyball" -> cantUsers.text = countDocument?.volleyball.toString() + " users"
                "Gym" -> cantUsers.text = countDocument?.gym.toString() + " users"
            }
        }
    }

    private fun loadBackButton() {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = inflater.inflate(R.layout.back_button, null)
        parentFrameLayout = findViewById(R.id.frameLayout)
        parentFrameLayout!!.addView(view, 0)

        backButton = view.findViewById(R.id.imageButton)
        backButton?.setOnClickListener {
            finish()
        }
    }
}
