package com.example.athletesapp.views


import android.content.Context
import android.content.Intent

import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation

import com.example.athletesapp.*
import com.example.athletesapp.databinding.ActivityMainBinding
import com.example.athletesapp.models.User
import com.example.athletesapp.utils.MyCache
import com.example.athletesapp.viewmodels.LoggedInViewModel
import com.example.athletesapp.viewmodels.UserViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseUser

class LoggedInFragment : Fragment() {
    private var loggedInUserTextView: TextView? = null
    private var goodAfternoonTextView: TextView? = null
    private var profilePictureImageView: ImageView? = null
    private var loggedInViewModel: LoggedInViewModel? = null

    private var actualUser = User()
    private val userViewModel: UserViewModel by viewModels()

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loggedInViewModel = ViewModelProviders.of(this).get(LoggedInViewModel::class.java)
        loggedInViewModel!!.userLiveData.observe(this) {
            firebaseUser -> onSetLoginState(firebaseUser)
        }
        loggedInViewModel!!.loggedOutLiveData.observe(this){
            loggedOut -> onLogOut(loggedOut)
        }
        userViewModel!!.getUserPpData.observe(this) { bitmap ->
            activity?.runOnUiThread(java.lang.Runnable {
                if(bitmap!=null)
                {
                    profilePictureImageView?.setImageBitmap(bitmap)
                    MyCache.instance.saveBitmapToCache("profilepicture", bitmap!!)
                }
            })
        }
        loggedInViewModel!!.progressEncounter.observe(this, Observer { preferences ->
            val sport = preferences.getString("sport",null)
            val user = preferences.getString("user",null)
            if (sport!=null && user == null)
            {
                val snackbar: Snackbar = Snackbar
                    .make(requireActivity().getWindow().getDecorView().findViewById(android.R.id.content), "Your " + sport + " encounter is missing an oponent!", 5000)

                snackbar.setAction("Find one!",{
                    val intent = Intent(requireContext(), UsersListActivity::class.java)
                    intent.putExtra("sport",sport)
                    val userEmail = loggedInViewModel!!.getActiveUserEmail()
                    intent.putExtra("email", userEmail)
                    startActivity(intent)
                })
                snackbar.show()
            }
            else if (sport!=null && user !=null)
            {
                val snackbar: Snackbar = Snackbar
                    .make(requireActivity().getWindow().getDecorView().findViewById(android.R.id.content), "Your " + sport + " encounter with " + user + " is waiting!", 5000)

                snackbar.setAction("Wrap it up!",{
                    val intent = Intent(requireContext(), VenuesListActivity::class.java)
                    intent.putExtra("sport",sport)
                    intent.putExtra("user", user)
                    startActivity(intent)
                })
                snackbar.show()
            }
        })
        initViewModel()
    }

    override fun onStart() {
        super.onStart()
        val pp = MyCache.instance.retrieveBitmapFromCache("profilepicture")
        if(pp!=null){
            profilePictureImageView?.setImageBitmap(pp)
        }
    }

    private fun initViewModel() {
        userViewModel.getUserLiveData.observe(this) {
            onGetUser(it)
        }
    }

    private fun onSetLoginState(firebaseUser: FirebaseUser?){
        if (firebaseUser != null) {
            loggedInUserTextView!!.text = actualUser.name
            binding.logoutButton.isEnabled = true
        } else {
            binding.logoutButton.isEnabled = false
        }
    }

    private fun onLogOut(loggedOut: Boolean) {
        if (loggedOut) {
            Toast.makeText(context, "User Logged Out", Toast.LENGTH_SHORT).show()
            Navigation.findNavController(requireView())
                .navigate(R.id.action_loggedInFragment_to_loginRegisterFragment)
            val sharedPref = activity?.getSharedPreferences("progress_tracker",Context.MODE_PRIVATE) ?: return
            with (sharedPref.edit()) {
                clear()
                apply()
            }
        }
    }

    private fun onGetUser(it: User) {
        actualUser = it
        goodAfternoonTextView = binding.greetingLabel
        goodAfternoonTextView?.text = "Good afternoon " + actualUser.name
        profilePictureImageView = binding.imageView2
        val pp = MyCache.instance.retrieveBitmapFromCache("profilepicture")
            if(actualUser.pictureLocation.equals("offline"))
            {
                if(pp!=null){
                    profilePictureImageView?.setImageBitmap(pp)
                }
                else
                {
                    val snackbar: Snackbar = Snackbar
                        .make(requireActivity().getWindow().getDecorView().findViewById(android.R.id.content), "There was a problem connecting you to the internet. Some features might not be available", Snackbar.LENGTH_LONG)
                    snackbar.show()
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ActivityMainBinding.inflate(inflater, container, false)
        val view = binding.root
        loggedInUserTextView = binding.userLabel

        val userEmail = loggedInViewModel!!.getActiveUserEmail()
        userViewModel.getOneById(userEmail)

        binding.logoutButton.setOnClickListener(View.OnClickListener { loggedInViewModel!!.logOut() })
        binding.imageView2.setOnClickListener(View.OnClickListener {
            val intent = Intent(requireContext(), ProfileActivity::class.java)
            intent.putExtra("user", actualUser)
            startActivity(intent)
        })

        binding.findPartnerButton.setOnClickListener(View.OnClickListener {
            val intent = Intent(requireContext(), SportsListActivity::class.java)
            intent.putExtra("favSport", actualUser.favSport)
            intent.putExtra("email", userEmail)
            startActivity(intent)
        })

        return view
    }
}
