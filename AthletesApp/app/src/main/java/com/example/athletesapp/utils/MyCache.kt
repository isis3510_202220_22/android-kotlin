package com.example.athletesapp.utils

import android.graphics.Bitmap
import androidx.collection.LruCache

class MyCache private constructor() {
    private val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()
    private val cacheSize = maxMemory / 8

    private object HOLDER {
        val INSTANCE = MyCache()
    }

    companion object {
        val instance: MyCache by lazy { HOLDER.INSTANCE }
    }
    private val lru: LruCache<Any, Any> = LruCache(cacheSize)

    fun saveBitmapToCache(key: String, bitmap: Bitmap) {
        try {
            MyCache.instance.lru.put(key, bitmap)
        } catch (e: Exception) {
        }
    }

    fun retrieveBitmapFromCache(key: String): Bitmap? {
        try {
            return MyCache.instance.lru.get(key) as Bitmap?
        } catch (e: Exception) {
        }
        return null
    }
}
