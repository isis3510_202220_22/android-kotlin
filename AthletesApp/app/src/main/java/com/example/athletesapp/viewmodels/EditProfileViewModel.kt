package com.example.athletesapp.viewmodels

import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.athletesapp.models.repositories.AuthAppRepository
import com.example.athletesapp.models.repositories.UserRepository

class EditProfileViewModel (application: Application) : AndroidViewModel(application) {

    var userRepository: UserRepository
    val getUserNewPp: MutableLiveData<Bitmap>
    var authAppRepository: AuthAppRepository

    fun resetStats(id: String?){
        userRepository.resetStats(id)
    }

    fun updateName(id:String?, newName:String?)
    {
        userRepository.updateName(newName,id)
    }

    fun changePassword(id:String?,oldPassword:String,newPassword:String)
    {
        authAppRepository.updatePassword(id+"",oldPassword,newPassword)
    }

    fun processImage(bitmap: Bitmap, userId: String?){
        userRepository.processImage(bitmap, userId)
    }

    init {
        userRepository = UserRepository(application)
        authAppRepository = AuthAppRepository(application)
        getUserNewPp = userRepository.getUserNewPp
    }
}
