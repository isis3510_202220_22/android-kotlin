package com.example.athletesapp.models

import java.io.Serializable

data class LookingFor (
    var id: String? = null,
    var football: Boolean? = null,
    var basketball: Boolean? = null,
    var tennis: Boolean? = null,
    var volleyball: Boolean? = null,
    var gym: Boolean? = null,
    var email: String? = null,
    ): Serializable {
        fun toMap(): Map<String,Any?>{
            return mapOf(
                "id" to id,
                "football" to football,
                "basketball" to basketball,
                "tennis" to tennis,
                "volleyball" to volleyball,
                "gym" to gym,
                "email" to email,
            )
        }
}
