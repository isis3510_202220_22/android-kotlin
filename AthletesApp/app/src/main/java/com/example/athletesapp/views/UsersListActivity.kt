package com.example.athletesapp.views

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.athletesapp.R
import com.example.athletesapp.models.LookingFor
import com.example.athletesapp.models.User
import com.example.athletesapp.utils.ConnectionProvider
import com.example.athletesapp.utils.MyCache
import com.example.athletesapp.viewmodels.LookingForViewModel
import com.example.athletesapp.viewmodels.UserViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.URL

class UsersListActivity : AppCompatActivity() {
    private var parentLinearLayout: LinearLayout? = null
    private var parentFrameLayout: FrameLayout? = null
    private var backButton: ImageButton? = null
    private val lookingForViewModel: LookingForViewModel by viewModels()
    private val userViewModel: UserViewModel by viewModels()
    var connectivityAvailable: Boolean = false
    private lateinit var nextPageButton: Button
    private lateinit var prevPageButton: Button
    private lateinit var selectedSport: String
    private var emails = arrayListOf<LookingFor>()
    private var pageNum = 0
    private var pageSize = 8
    private lateinit var connectionProvider: ConnectionProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        connectionProvider = ConnectionProvider(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users_list)

        parentLinearLayout = findViewById(R.id.linearLayout)

        selectedSport = intent.getStringExtra("sport").toString()
        userViewModel.saveSportProgress(intent.getStringExtra("sport")?.lowercase())
        loadBackButton()

        if(connectionProvider.connectivityAvailable()) {
            selectedSport.let {
                lookingForViewModel.getBySport(
                    it,
                    intent.getStringExtra("email")!!
                )
            }

            initUserViewModel()
            initLookingForViewModel()

            prevPageButton = findViewById(R.id.previousPageBut2)
            nextPageButton = findViewById(R.id.nextPageBut2)

            prevPageButton.isEnabled = false
            nextPageButton.isEnabled = false

            prevPageButton.setOnClickListener {
                connectivityAvailable = isInternetAvailable(this)
                if (connectivityAvailable) {
                    pageNum -= 1
                    if (pageNum == 0) prevPageButton.isEnabled = false
                    if (((pageNum + 1) * pageSize) + 1 < emails.size) nextPageButton.isEnabled =
                        true
                    userViewModel.getListByIds(
                        ArrayList(
                            emails.subList(
                                pageNum * pageSize,
                                emails.size.coerceAtMost(((pageNum + 1) * pageSize) - 1)
                            )
                        )
                    )
                } else {
                    val snackbar: Snackbar = Snackbar
                        .make(
                            window.decorView.findViewById(android.R.id.content),
                            "You don't have an active internet connection. Please try again later",
                            Snackbar.LENGTH_LONG
                        )
                    snackbar.show()
                }
            }

            nextPageButton.setOnClickListener {
                connectivityAvailable = isInternetAvailable(this)
                if (connectivityAvailable) {
                    pageNum += 1
                    if (pageNum >= 0) prevPageButton.isEnabled = true
                    if (((pageNum + 1) * pageSize) + 1 > emails.size) nextPageButton.isEnabled =
                        false
                    userViewModel.getListByIds(
                        ArrayList(
                            emails.subList(
                                pageNum * pageSize,
                                emails.size.coerceAtMost(((pageNum + 1) * pageSize) - 1)
                            )
                        )
                    )
                } else {
                    val snackbar: Snackbar = Snackbar
                        .make(
                            window.decorView.findViewById(android.R.id.content),
                            "You don't have an active internet connection. Please try again later",
                            Snackbar.LENGTH_LONG
                        )
                    snackbar.show()
                }
            }
        }
        else {
            setContentView(R.layout.connection_error)
        }
    }

    private fun initUserViewModel() {
        userViewModel.getUsersByIdsLiveData.observe(this) {
            onGetUsers(it)
        }
    }

    private fun initLookingForViewModel() {
        lookingForViewModel.getLookingForsBySportLiveData.observe(this) {
            emails = it
            if (((pageNum+1)*pageSize)+1 <= emails.size) nextPageButton.isEnabled = true
            userViewModel.getListByIds(ArrayList(emails.subList(pageNum*8,
                emails.size.coerceAtMost(((pageNum + 1) * 8) - 1)
            )))
        }
    }

    private fun getImageOnline(pictureLocation: String, icon: ImageView, name: String) {
        GlobalScope.launch(Dispatchers.IO) {
            try{
                val newurl = URL(pictureLocation)
                val mIconVal = BitmapFactory.decodeStream(newurl.openConnection().getInputStream())

                runOnUiThread {
                    if (mIconVal != null) {
                        icon.setImageBitmap(mIconVal)
                        MyCache.instance.saveBitmapToCache(name, mIconVal)
                    }
                }
            }
            catch (e: Exception){
            }
        }
    }

    private fun onGetUsers(it: ArrayList<User>) {
        val inflater =
            getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var rowView: View?
        var name: TextView?
        var icon: ImageView?
        var cantUsers: TextView?
        var arrow: ImageView?

        val layout: ViewGroup = findViewById(R.id.linearLayout)
        layout.removeAllViews()

        for (i in it) {
            rowView = inflater.inflate(R.layout.user_card, null)

            rowView.setOnClickListener {
                val intent = Intent(this, AnotherUserDetailActivity::class.java)
                intent.putExtra("user", i)
                startActivity(intent)
            }

            parentLinearLayout!!.addView(rowView, 0)

            arrow = findViewById(R.id.imageView14)
            arrow.setOnClickListener {
                val intent = Intent(this, VenuesListActivity::class.java)
                intent.putExtra("user", i.email)
                intent.putExtra("sport", selectedSport.lowercase())
                startActivity(intent)
            }

            name = findViewById(R.id.textView2)
            name.text = i.name

            icon = findViewById(R.id.imageView10)

            val image = i.pictureLocation?.let { it1 -> MyCache.instance.retrieveBitmapFromCache(it1) }
            if (image != null) {
                icon.setImageBitmap(image)
            } else {
                if (connectionProvider.connectivityAvailable()) {
                    if (i.pictureLocation?.startsWith("@") == true) {
                        icon.setImageResource(R.mipmap.ic_user_icon)
                    }
                    else {
                        i.pictureLocation?.let { it1 ->
                            i.name?.let { it2 ->
                                getImageOnline(
                                    it1, icon,
                                    it2
                                )
                            }
                        }
                    }
                } else {
                    icon.setImageResource(R.mipmap.ic_user_icon)
                }
            }

            cantUsers = findViewById(R.id.textView8)
            cantUsers.text = i.username
        }
    }

    private fun loadBackButton() {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = inflater.inflate(R.layout.back_button, null)
        parentFrameLayout = findViewById(R.id.frameLayout)
        parentFrameLayout!!.addView(view, 0)

        backButton = view.findViewById(R.id.imageButton)
        backButton?.setOnClickListener {
            finish()
        }
    }

    private fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    return true
                }
            }
        }
        return false
    }
}
