package com.example.athletesapp.viewmodels

import android.app.Application
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.athletesapp.models.repositories.AuthAppRepository
import com.example.athletesapp.models.repositories.MatchRepository
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.*

class LoggedInViewModel(application: Application) : AndroidViewModel(application) {
    private val authAppRepository: AuthAppRepository
    private val matchRepository:MatchRepository
    val userLiveData: MutableLiveData<FirebaseUser?>
    val loggedOutLiveData: MutableLiveData<Boolean>
    val progressEncounter: MutableLiveData<SharedPreferences>

    private val job = Job()

    fun logOut() {
        authAppRepository.logOut()
    }

    fun getActiveUserEmail() : String? {
        return authAppRepository.getActiveUserEmail()
    }

    init {
        authAppRepository = AuthAppRepository(application)
        userLiveData = authAppRepository.userLiveData
        loggedOutLiveData = authAppRepository.loggedOutLiveData
        matchRepository = MatchRepository(application)
        progressEncounter = matchRepository.progressEncounter
        matchRepository.publishProgress()
    }
}
