package com.example.athletesapp.models.repositories

import android.app.Application
import android.graphics.*
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.athletesapp.models.LookingFor
import com.example.athletesapp.models.User
import com.example.athletesapp.utils.ConnectionProvider
import com.example.athletesapp.utils.MyCache
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL


class UserRepository(private val application: Application) {
    private var db = FirebaseFirestore.getInstance()
    private val collection:String = "users"
    private var connectionProvider = ConnectionProvider(application.applicationContext)
    private val client = OkHttpClient()
    var user = FirebaseAuth.getInstance().currentUser
    private var matchRepository: MatchRepository = MatchRepository(application)

    val getUserLiveData: MutableLiveData<User> by lazy {
        MutableLiveData<User>()
    }

    val getUserPpData: MutableLiveData<Bitmap> by lazy {
        MutableLiveData<Bitmap>()
    }

    val getUsersByIdsLiveData: MutableLiveData<ArrayList<User>> by lazy {
        MutableLiveData<ArrayList<User>>()
    }

    val getUserNewPp: MutableLiveData<Bitmap> by lazy {
        MutableLiveData<Bitmap>()
    }

    fun getOneById(id: String?) {
        db.collection(collection)
            .whereEqualTo("email", id)
            .get()
            .addOnSuccessListener { users ->
                val actualUser = User()
                for (user in users) {
                    actualUser.name = user.id
                    actualUser.name = user.data["name"] as String?
                    actualUser.username = user.data["username"] as String?
                    actualUser.cantFriends = user.data["cantFriends"] as Number?
                    actualUser.cantMatches = user.data["cantMatches"] as Number?
                    actualUser.favSport = user.data["favSport"] as String?
                    actualUser.cantMatchesFavSport = user.data["cantMatchesFavSport"] as Number?
                    if(connectionProvider.connectivityAvailable()){
                        actualUser.pictureLocation = user.data["pictureLocation"] as String?
                        actualUser.pictureLocation?.let { getImageOnline(it) }
                    }
                    else{
                        actualUser.pictureLocation = "offline"
                    }
                    actualUser.email = user.data["email"] as String?
                }
                getUserLiveData.postValue(actualUser)
                matchRepository.saveActualuser(actualUser.email)
            }.addOnFailureListener{
                Log.d("get", it.localizedMessage!!)
            }
    }

    fun getListByIds(ids: ArrayList<LookingFor>) {
        var emails = arrayListOf<String>()
        for (i in ids) {
            i.email?.let { emails.add(it) }
        }

        db.collection(collection)
            .whereIn("email", emails)
            .get()
            .addOnSuccessListener { users ->
                val listUser = arrayListOf<User>()
                for (user in users) {
                    val actualUser = User()
                    actualUser.name = user.id
                    actualUser.name = user.data["name"] as String?
                    actualUser.username = user.data["username"] as String?
                    actualUser.cantFriends = user.data["cantFriends"] as Number?
                    actualUser.cantMatches = user.data["cantMatches"] as Number?
                    actualUser.favSport = user.data["favSport"] as String?
                    actualUser.cantMatchesFavSport = user.data["cantMatchesFavSport"] as Number?
                    actualUser.pictureLocation = user.data["pictureLocation"] as String?
                    actualUser.email = user.data["email"] as String?
                    listUser.add(actualUser)
                }
                getUsersByIdsLiveData.postValue(listUser)
            }.addOnFailureListener {
                Log.d("get", it.localizedMessage!!)
            }
    }

    fun addEmailToPreferences(email: String)
    {

    }

    private fun getImageOnline(pictureLocation: String){

        GlobalScope.launch(Dispatchers.IO) {
            try{
                val newurl = URL(pictureLocation)
                var mIcon_val = BitmapFactory.decodeStream(newurl.openConnection().getInputStream())
                getUserPpData.postValue(mIcon_val)
            }
            catch (e: Exception){
            }
        }
    }

    fun processImage(bitmap: Bitmap, userId: String?){
        getUserNewPp.postValue(getRoundedCroppedBitmap(bitmap))
        val outputDir: File = application.applicationContext.getCacheDir() // context being the Activity pointer
        val outputFile: File = File.createTempFile("ppc", ".png", outputDir)
        val output: FileOutputStream = FileOutputStream(outputFile)
        getRoundedCroppedBitmap(bitmap)?.compress(Bitmap.CompressFormat.PNG,100, output)
        GlobalScope.launch(Dispatchers.IO) {
            run(outputFile, userId)
        }
        MyCache.instance.saveBitmapToCache("profilepicture",getRoundedCroppedBitmap(bitmap)!!)
    }

    fun run(file: File, userId: String?) {
        val body = RequestBody.create("image/jpeg".toMediaTypeOrNull(), file)
        val request = Request.Builder()
            .url("https://ymp84k8z21.execute-api.us-east-1.amazonaws.com/v1/athletim/pp$userId")
            .put(body)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {println(e.stackTrace)}
            override fun onResponse(call: Call, response: Response) {updateProfilePic(userId) }
        })
    }

    private fun updateProfilePic(userId: String?) {
        val docs = db.collection("users").whereEqualTo("email",userId).get().addOnSuccessListener {
                documents -> for (document in documents){
            val list = userId?.split("@")
            db.collection("users").document(document.id).update("pictureLocation", "https://athletim.s3.amazonaws.com/pp"+list?.get(0)+"%40"+list?.get(1))
        }
        }
    }

    private fun getRoundedCroppedBitmap(bitmap: Bitmap): Bitmap? {
        val widthLight = bitmap.width
        val heightLight = bitmap.height
        val output = Bitmap.createBitmap(
            bitmap.width, bitmap.height,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(output)
        val paintColor = Paint()
        paintColor.setFlags(Paint.ANTI_ALIAS_FLAG)
        val rectF = RectF(Rect(0, 0, widthLight, heightLight))
        canvas.drawCircle((widthLight/2).toFloat(),(heightLight/2).toFloat(),(widthLight/2).toFloat(),paintColor)
        val paintImage = Paint()
        paintImage.setXfermode(PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP))
        canvas.drawBitmap(bitmap, 0.toFloat(), 0.toFloat(), paintImage)
        return output
    }

    fun updateName(newName: String?, id: String?){
        db.collection(collection)
            .whereEqualTo("email", id)
            .get()
            .addOnSuccessListener { users ->
                val actualUser = User()
                for (user in users) {
                    db.collection(collection).document(user.id).update("name",newName)
                }
            }
    }

    fun resetStats(id: String?){
        db.collection(collection)
            .whereEqualTo("email", id)
            .get()
            .addOnSuccessListener { users ->
                val actualUser = User()
                for (user in users) {
                    db.collection(collection).document(user.id).update("cantMatches",0)
                    db.collection(collection).document(user.id).update("cantMatchesFavSport",0)
                }
            }
    }
}
