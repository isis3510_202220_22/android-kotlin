package com.example.athletesapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.athletesapp.databinding.ActivityMainBinding
import com.example.athletesapp.views.AuthActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        finish()
        startActivity(Intent(this, AuthActivity::class.java))
    }
}
