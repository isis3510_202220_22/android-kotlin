package com.example.athletesapp.models

import com.google.firebase.firestore.GeoPoint

data class Venue(
    var address: String? = null,
    var description: String? = null,
    var img: String? = null,
    var location: GeoPoint? = null,
    var name: String? = null,
    var rating: Number? = null,
    var distance: Double? = null,
    var thumbnail: String? = null
){
    fun toMap(): Map<String,Any?>{
        return mapOf(
            "address" to address,
            "name" to name,
            "description" to description,
            "rating" to rating,
            "location" to location
        )
    }

    override fun toString(): String {
        return "[NAME:${name}, ADDRESS:${address}, LOCATION:${location}, DISTANCE:${distance}]"
    }
}
