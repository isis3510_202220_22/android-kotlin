package com.example.athletesapp.viewmodels

import android.app.Application
import android.graphics.*
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.athletesapp.models.LookingFor
import com.example.athletesapp.models.repositories.MatchRepository
import com.example.athletesapp.models.repositories.UserRepository
import com.example.athletesapp.models.User

class UserViewModel(application: Application): AndroidViewModel(application) {
    private val userRepository: UserRepository
    private val matchRepository = MatchRepository(application)
    val getUserLiveData: MutableLiveData<User>
    val getUserPpData: MutableLiveData<Bitmap>
    val getUsersByIdsLiveData: MutableLiveData<ArrayList<User>>

    fun getOneById(id: String?) {
        userRepository.getOneById(id)
    }

    fun getListByIds(ids: ArrayList<LookingFor>) {
        userRepository.getListByIds(ids)
    }

    fun saveCurrentUser(email: String?)
    {
        matchRepository.saveActualuser(email)
    }

    fun removePreferences(){
        matchRepository.removePreferences()
    }

    fun saveVenue(venue: String?)
    {
        matchRepository.saveVenueProgress(venue)
    }

    fun saveSportProgress(sport: String?)
    {
        matchRepository.saveSportProgress(sport)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun registerMatch()
    {
        matchRepository.registerMatch()
    }

    init {
        userRepository = UserRepository(application)
        getUserLiveData = userRepository.getUserLiveData
        getUserPpData = userRepository.getUserPpData
        getUsersByIdsLiveData = userRepository.getUsersByIdsLiveData
    }
}
