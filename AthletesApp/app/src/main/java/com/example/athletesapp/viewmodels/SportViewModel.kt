package com.example.athletesapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.athletesapp.models.Sport
import com.example.athletesapp.models.repositories.LookingForRepository
import com.example.athletesapp.models.repositories.SportRepository

class SportViewModel (application: Application): AndroidViewModel(application) {
    private val sportRepository: SportRepository = SportRepository(application)

    fun getSports(): ArrayList<Sport> {
        return sportRepository.getSports()
    }

    fun getSportsByName (name: String): Sport {
        return sportRepository.getSportByName(name)
    }
}