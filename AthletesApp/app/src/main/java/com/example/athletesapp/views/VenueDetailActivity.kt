package com.example.athletesapp.views

import android.content.ContentValues
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.athletesapp.R
import com.example.athletesapp.databinding.ActivityVenueDetailBinding
import com.example.athletesapp.models.Venue
import com.example.athletesapp.models.repositories.MatchRepository
import com.example.athletesapp.utils.ConnectionProvider
import com.example.athletesapp.utils.MyCache
import com.example.athletesapp.viewmodels.UserViewModel
import com.example.athletesapp.viewmodels.VenueViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL
import java.time.Duration

class VenueDetailActivity : AppCompatActivity() {
    private lateinit var venueNameTextView: TextView
    private lateinit var venuePicture: ImageView
    private lateinit var ratingTextView: TextView
    private lateinit var addressTextView: TextView
    private lateinit var descriptionTextView: TextView
    private lateinit var openMapButton: Button
    private val collectionMatches:String = "matches"
    private var db = FirebaseFirestore.getInstance()

    private var buttonFrameLayout: FrameLayout? = null
    private var backButton: ImageButton? = null

    private lateinit var venue: Venue
    private lateinit var connectionProvider: ConnectionProvider
    private lateinit var userViewModel: UserViewModel
    private lateinit var venueViewModel: VenueViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityVenueDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        venueNameTextView = binding.venueNameText
        venuePicture = binding.venuePicture
        ratingTextView = binding.ratingText
        addressTextView = binding.addresText
        descriptionTextView = binding.descriptionText
        openMapButton = binding.openMapButton
        loadBackButton()

        connectionProvider = ConnectionProvider(this)

        userViewModel = UserViewModel(this.application)

        venue = loadVenue()
        openMapButton.setOnClickListener {
            if (!connectionProvider.connectivityAvailable()){
                showWarning()
            } else {
                launchDirections()
            }
        }

        venueViewModel = VenueViewModel(this.application)

        venueViewModel!!.progressEncounter.observe(this){
            status ->
            if(status)
            {
                Snackbar.make(binding.root, "Match successfully registered", Snackbar.LENGTH_LONG).show()
            }
        }


        binding.registerMatchButton.setOnClickListener {
            try {
                val newFragment = DatePickerFragment(application)
                TimePickerFragment(application, binding.root).show(
                    supportFragmentManager,
                    "timePicker"
                )
                newFragment.show(supportFragmentManager, "datePicker")
            } catch (e: Exception) {
                Snackbar.make(
                    binding.root,
                    "There was an error registering your match. Try again later.",
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun loadBackButton() {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = inflater.inflate(R.layout.back_button, null)
        buttonFrameLayout = findViewById(R.id.backButtonFrame)
        buttonFrameLayout!!.addView(view, 0)

        backButton = view.findViewById(R.id.imageButton)
        backButton?.setOnClickListener(View.OnClickListener {
            finish()
        })
    }

    private fun loadVenue(): Venue{
        val address = intent.getStringExtra("address")
        val description = intent.getStringExtra("description")
        val img = intent.getStringExtra("img")
        val latitude = intent.getDoubleExtra("latitude",0.0)
        val longitude = intent.getDoubleExtra("longitude",0.0)
        val name = intent.getStringExtra("name")
        userViewModel.saveVenue(intent.getStringExtra("name"))
        val rating = intent.getDoubleExtra("rating",0.0)

        venueNameTextView.text = name
        ratingTextView.text = rating.toString()
        addressTextView.text = address
        descriptionTextView.text = description
        //getting pic from S3 bucket
        GlobalScope.launch(Dispatchers.IO) {
            getImage(img)
        }

        return Venue(address, description, img, GeoPoint(latitude, longitude),name, rating)
    }

    private fun launchDirections() {
        val gmmIntentUri =
            Uri.parse("https://www.google.com/maps/dir/?api=1&destination=" + Uri.encode("${venue.location!!.latitude},${venue.location!!.longitude}"))
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        startActivity(mapIntent)
    }

    private fun showWarning(){
        var builder = AlertDialog.Builder(this)
        builder.setTitle("Warning")
        builder.setMessage("We cannot detect your connection. Directions may not be available. Are you sure you want to continue?")
        builder.setPositiveButton("Continue", DialogInterface.OnClickListener { dialog, i ->
            launchDirections()
            dialog.cancel()
        })
        builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, i ->
            dialog.cancel()
        })

        var alertDialog = builder.create()
        alertDialog.show()
    }


    private suspend fun getImage(imgUrl: String?) {
        var img = MyCache.instance.retrieveBitmapFromCache(imgUrl!!)
        // Img found in cache
        if (img != null){
            withContext(Dispatchers.Main){
                venuePicture.setImageBitmap(img)
            }
        }
        // Img not found in cache
        else {
            try {
                val newUrl = URL(imgUrl)
                img = BitmapFactory.decodeStream(newUrl.openConnection().getInputStream())
                MyCache.instance.saveBitmapToCache(imgUrl,img) //Saving img to LRU cache
                withContext(Dispatchers.Main){
                    venuePicture.setImageBitmap(img)
                }
            }
            catch (e: Exception){
                withContext(Dispatchers.Main){
                    Toast.makeText(venuePicture.context, "No connection detected. Image could not be downloaded.", Toast.LENGTH_LONG).show()
                    venuePicture.setImageResource(R.drawable.broken_image)
                }
            }
        }

    }

}