package com.example.athletesapp.views

import android.content.Context
import android.content.Intent
import android.graphics.*
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.ViewModelProviders
import com.example.athletesapp.MainActivity
import com.example.athletesapp.R
import com.example.athletesapp.databinding.ActivityEditProfileBinding
import com.example.athletesapp.utils.ConnectionProvider
import com.example.athletesapp.utils.MyCache
import com.example.athletesapp.viewmodels.EditProfileViewModel

class EditProfileActivity : AppCompatActivity() {
    private lateinit var profilePicture: ImageView
    var editUserProfileViewModel: EditProfileViewModel?= null

    val REQUEST_IMAGE_CAPTURE = 1
    var userId: String? = null
    private lateinit var connectionProvider: ConnectionProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionProvider = ConnectionProvider(this)


        if(!connectionProvider.connectivityAvailable())
        {
            loadFallbackScreen()
        }
        else {
            setContentView(R.layout.activity_edit_profile)
            val binding = ActivityEditProfileBinding.inflate(layoutInflater)
            setContentView(binding.root)
            profilePicture = binding.imageView7

            val pp = MyCache.instance.retrieveBitmapFromCache("profilepicture")
            if (pp != null) {
                profilePicture?.setImageBitmap(pp)
            }

            binding.userLabel2.text = intent.getStringExtra("userName")
            userId = intent.getStringExtra("userId")
            binding.uploadPpButton.setOnClickListener({
                dispatchTakePictureIntent()
            })
            editUserProfileViewModel =
                ViewModelProviders.of(this).get(EditProfileViewModel::class.java)
            editUserProfileViewModel!!.getUserNewPp.observe(this) { bitmap ->
                binding.imageView7.setImageBitmap(bitmap)
            }

            binding.updateNameButton.setOnClickListener() {
                editUserProfileViewModel!!.updateName(
                    intent.getStringExtra("userId"),
                    binding.newNameTextField.text.toString()
                )
                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
            }

            binding.resetStatsButton.setOnClickListener() {
                editUserProfileViewModel!!.resetStats(intent.getStringExtra("userId"))
                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
            }

            binding.passwordChangeButton.setOnClickListener() {
                editUserProfileViewModel!!.changePassword(
                    intent.getStringExtra("userId"),
                    binding.oldPasswordTextField.text.toString(),
                    binding.newPasswordTextField.text.toString()
                )
            }
        }

    }

    private fun loadFallbackScreen() {
        setContentView(R.layout.connection_error)
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            editUserProfileViewModel!!.processImage(imageBitmap,userId)
        }
    }
}
