package com.example.athletesapp.models.repositories

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData

class UserRegisterRepository(private val application: Application) {
    val sharedPref = application.getSharedPreferences("register_preferences", Context.MODE_PRIVATE)

    val registerPreferences: MutableLiveData<SharedPreferences> by lazy {
        MutableLiveData<SharedPreferences>()
    }

    fun removePreferences()
    {
        val sharedPref = application.getSharedPreferences("register_preferences", Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            clear()
            apply()
        }
        registerPreferences.postValue(sharedPref)
    }

    fun updatePreferences(email: String?,password: String?,name: String?, username: String?, favSport: Int){
        val sharedPref = application.getSharedPreferences("register_preferences",Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString("email", email)
            putString("password", password)
            putString("name", name)
            putString("username",username)
            putInt("favSport",favSport)
            apply()
        }
    }

    fun getRegistrationPreferences()
    {
        registerPreferences.postValue(sharedPref)
    }
}
