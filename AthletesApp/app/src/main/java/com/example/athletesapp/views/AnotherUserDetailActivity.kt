package com.example.athletesapp.views

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.athletesapp.R
import com.example.athletesapp.databinding.ActivityAnotherUserDetailBinding
import com.example.athletesapp.models.Sport
import com.example.athletesapp.models.User
import com.example.athletesapp.utils.MyCache
import com.example.athletesapp.viewmodels.SportViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.Console
import java.net.URL

class AnotherUserDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAnotherUserDetailBinding
    private var parentFrameLayout: FrameLayout? = null
    private var backButton: ImageButton? = null
    private var profilePictureImageView: ImageView? = null
    private var favsportPictureImageView: ImageView? = null
    private var favoriteSport: TextView? = null
    private var actualUser : User? = null
    private val sportViewModel: SportViewModel by viewModels()
    private var actualFavSport: Sport? = null
    var connectivityAvailable: Boolean = false;

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        connectivityAvailable = isInternetAvailable(this)

        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_another_user_detail)
        actualUser = intent.getSerializableExtra("user") as? User
        if (actualUser?.cantFriends == null) actualUser?.cantFriends = 0
        if (actualUser?.cantMatches == null) actualUser?.cantMatches = 0
        if (actualUser?.cantMatchesFavSport == null) actualUser?.cantMatchesFavSport = 0
        binding.user = actualUser

        actualFavSport = actualUser?.favSport?.let { sportViewModel.getSportsByName(it) }

        if (actualUser != null) {
            profilePictureImageView = findViewById(R.id.imageView7)
            favsportPictureImageView = findViewById(R.id.imageView4)

            val pp = actualUser!!.name?.let { MyCache.instance.retrieveBitmapFromCache(it) }
            if(pp!=null){
                binding.imageView7.setImageBitmap(getRoundedCroppedBitmap(pp))
            }

            val image = actualFavSport?.name?.let { MyCache.instance.retrieveBitmapFromCache(it.lowercase() + "black") }
            if(image!=null){
                favsportPictureImageView?.setImageBitmap(image)
            }
            else {
                if (connectivityAvailable) {
                    actualFavSport?.let { getFavSportImageOnline(it.blackIconFile) }
                } else {
                    val pictureID = resources.getIdentifier(
                        "@mipmap/ic_default_sports",
                        "drawable",
                        packageName
                    )
                    pictureID.let { favsportPictureImageView?.setImageResource(it) }
                    val snackbar: Snackbar = Snackbar
                        .make(
                            getWindow().getDecorView().findViewById(android.R.id.content),
                            "There was a problem connecting you to the internet. Some features might not be available",
                            Snackbar.LENGTH_LONG
                        )
                    snackbar.show()
                }
            }
        }

        favoriteSport = findViewById(R.id.textView7)
        if (actualUser?.favSport == null) {
            favoriteSport?.text = "Football"
        }
        else {
            favoriteSport?.text = actualUser?.favSport!![0].uppercaseChar() + actualUser?.favSport!!.drop(1)

        }

        loadBackButton()
    }

    private fun loadBackButton() {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = inflater.inflate(R.layout.back_button, null)
        parentFrameLayout = findViewById(R.id.frameLayout)
        parentFrameLayout!!.addView(view, 0)

        backButton = view.findViewById(R.id.imageButton)
        backButton?.setOnClickListener {
            finish()
        }
    }

    private fun getRoundedCroppedBitmap(bitmap: Bitmap): Bitmap? {
        val widthLight = bitmap.width
        val heightLight = bitmap.height
        val output = Bitmap.createBitmap(
            bitmap.width, bitmap.height,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(output)
        val paintColor = Paint()
        paintColor.setFlags(Paint.ANTI_ALIAS_FLAG)
        val rectF = RectF(Rect(0, 0, widthLight, heightLight))
        canvas.drawCircle((widthLight/2).toFloat(),(heightLight/2).toFloat(),(widthLight/2).toFloat(),paintColor)
        val paintImage = Paint()
        paintImage.setXfermode(PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP))
        canvas.drawBitmap(bitmap, 0.toFloat(), 0.toFloat(), paintImage)
        return output
    }

    fun getFavSportImageOnline(url: String){
        GlobalScope.launch(Dispatchers.IO) {
            try
            {
                val newurl = URL(url)
                var mIcon_val = BitmapFactory.decodeStream(newurl.openConnection().getInputStream())

                runOnUiThread(java.lang.Runnable {
                    if(mIcon_val!=null)
                    {
                        favsportPictureImageView?.setImageBitmap(mIcon_val)
                        actualFavSport?.name?.let { MyCache.instance.saveBitmapToCache(it.lowercase() + "black", mIcon_val) }

                    }
                })
            }
            catch(e: Exception){
            }
        }
    }

    fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }
}