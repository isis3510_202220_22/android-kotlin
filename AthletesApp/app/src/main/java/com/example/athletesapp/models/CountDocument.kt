package com.example.athletesapp.models

import java.io.Serializable

data class CountDocument (
    var id: String? = null,
    var football: Number? = null,
    var basketball: Number? = null,
    var tennis: Number? = null,
    var volleyball: Number? = null,
    var gym: Number? = null,
    ): Serializable {
        fun toMap(): Map<String,Any?>{
            return mapOf(
                "id" to id,
                "football" to football,
                "basketball" to basketball,
                "tennis" to tennis,
                "volleyball" to volleyball,
                "gym" to gym,
            )
        }
}
