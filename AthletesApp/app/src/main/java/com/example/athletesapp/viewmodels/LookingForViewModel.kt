package com.example.athletesapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.athletesapp.models.CountDocument
import com.example.athletesapp.models.LookingFor
import com.example.athletesapp.models.repositories.LookingForRepository

class LookingForViewModel(application: Application): AndroidViewModel(application) {
    private val lookingForRepository: LookingForRepository = LookingForRepository(application)
    val getCountDocumentLiveData: MutableLiveData<CountDocument> = lookingForRepository.getCountDocumentLiveData
    val getLookingForsBySportLiveData: MutableLiveData<ArrayList<LookingFor>> = lookingForRepository.getLookingForsBySportLiveData
    val getLookingForLiveData: MutableLiveData<LookingFor> = lookingForRepository.getLookingForLiveData

    fun getOneById(id: String?) {
        lookingForRepository.getOneById(id)
    }

    fun getBySport(sport: String, email: String) {
        lookingForRepository.getBySport(sport, email)
    }

    fun getCountDocument() {
        lookingForRepository.getCountDocument()
    }
}
