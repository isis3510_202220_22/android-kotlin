package com.example.athletesapp.models

data class Sport(val name: String, val defaultIconFile: String, val favIconFile: String, val blackIconFile: String)
