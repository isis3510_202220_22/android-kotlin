package com.example.athletesapp.views

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.athletesapp.R
import com.example.athletesapp.models.CountDocument
import com.example.athletesapp.models.LookingFor
import com.example.athletesapp.models.Sport
import com.example.athletesapp.utils.MyCache
import com.example.athletesapp.viewmodels.LookingForViewModel
import com.example.athletesapp.viewmodels.SportViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.*
import java.net.URL

class SetLookingForActivity : AppCompatActivity() {
    private var db = FirebaseFirestore.getInstance()
    private var parentLinearLayout: LinearLayout? = null
    private var parentFrameLayout: FrameLayout? = null
    private var backButton: ImageButton? = null
    var connectivityAvailable: Boolean = false
    private val lookingForViewModel: LookingForViewModel by viewModels()
    private val collectionLookingFor:String = "lookingFor"
    private var countDocument: CountDocument? = null
    private var counts: ArrayList<TextView> = arrayListOf()
    private val sportViewModel: SportViewModel by viewModels()
    private var sports: ArrayList<Sport> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        connectivityAvailable = isInternetAvailable(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_looking_for2)
        parentLinearLayout = findViewById(R.id.linearLayout)

        sports = sportViewModel.getSports()

        for (sport in sports) {
            if (sport.name == intent.getStringExtra("favSport")) {
                sports.remove(sport)
                sports.add(0, sport)
                break
            }
        }

        initViewModel()
        lookingForViewModel.getOneById(intent.getStringExtra("email"))
        loadBackButton()
    }

    private fun initViewModel() {
        lookingForViewModel.getLookingForLiveData.observe(this) {
            onGetLookingFor(it)
        }
        lookingForViewModel.getCountDocumentLiveData.observe(this) {
            onGetCountDocument(it)
        }
    }

    private fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    return true
                }
            }
        }
        return false
    }

    private fun getImageOnline(i: Int, icon: ImageView) {
        GlobalScope.launch(Dispatchers.IO) {
            try{
                val newurl: URL = if (i == 0) {
                    URL(sports[i].favIconFile)
                } else {
                    URL(sports[i].defaultIconFile)
                }

                val mIconVal = BitmapFactory.decodeStream(newurl.openConnection().getInputStream())

                runOnUiThread {
                    if (mIconVal != null) {
                        icon.setImageBitmap(mIconVal)
                        if (i == 0) {
                            MyCache.instance.saveBitmapToCache(
                                sports[i].name.lowercase() + "yellow",
                                mIconVal
                            )
                        } else {
                            MyCache.instance.saveBitmapToCache(
                                sports[i].name.lowercase() + "default",
                                mIconVal
                            )
                        }
                    }
                }
            }
            catch (e: Exception){
            }
        }
    }

    private fun onGetLookingFor(it: LookingFor) {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var rowView: View?
        var name: TextView?
        var icon: ImageView?
        var container: ConstraintLayout?
        var switch: SwitchCompat?
        val switches: ArrayList<SwitchCompat> = arrayListOf()

        for (i in sports.indices.reversed()) {
            rowView = inflater.inflate(R.layout.looking_for_sport_card, null)
            parentLinearLayout!!.addView(rowView, 0)

            name = findViewById(R.id.textView2)
            name.text = sports[i].name

            icon = findViewById(R.id.imageView10)

            val image: Bitmap? = if (i == 0) {
                MyCache.instance.retrieveBitmapFromCache(sports[i].name.lowercase() + "yellow")
            } else {
                MyCache.instance.retrieveBitmapFromCache(sports[i].name.lowercase() + "default")
            }
            if (image != null) {
                icon.setImageBitmap(image)
            } else {
                if (connectivityAvailable) {
                    getImageOnline(i, icon)
                } else {
                    icon.setImageResource(R.mipmap.ic_default_sports)
                }
            }

            if (i == 0) {
                container = findViewById(R.id.constraintLayout)
                container.setBackgroundResource(R.drawable.layout_border)
            }

            switch = findViewById(R.id.switch1)
            switches.add(switch)
            when(sports[i].name)
            {
                "Football" -> switch.isChecked = it.football == true
                "Basketball" -> switch.isChecked = it.basketball == true
                "Tennis" -> switch.isChecked = it.tennis == true
                "Volleyball" -> switch.isChecked = it.volleyball == true
                "Gym" -> switch.isChecked = it.gym == true
            }

            val cantUsers: TextView? = findViewById(R.id.textView8)
            if (cantUsers != null) {
                counts.add(cantUsers)
            }

            switch.setOnCheckedChangeListener { _, isChecked ->
                connectivityAvailable = isInternetAvailable(this)

                if(connectivityAvailable) {
                    it.id?.let { it1 ->
                        db.collection(collectionLookingFor).document(it1)
                            .update(sports[i].name.lowercase(), isChecked)
                    }
                    var previousVal = 0
                    when(sports[i].name)
                    {
                        "Football" -> previousVal = (countDocument?.football as Number).toInt()
                        "Basketball" -> previousVal = (countDocument?.basketball as Number).toInt()
                        "Tennis" -> previousVal = (countDocument?.tennis as Number).toInt()
                        "Volleyball" -> previousVal = (countDocument?.volleyball as Number).toInt()
                        "Gym" -> previousVal = (countDocument?.gym as Number).toInt()
                    }
                    val nextVal: Int = if(isChecked) {
                        previousVal+1
                    } else {
                        previousVal-1
                    }
                    countDocument?.id?.let { it1 ->
                        db.collection("sportsUsersCount").document(it1)
                            .update(sports[i].name.lowercase(), nextVal)
                    }
                    when(sports[i].name)
                    {
                        "Football" -> countDocument?.football = nextVal
                        "Basketball" -> countDocument?.basketball = nextVal
                        "Tennis" -> countDocument?.tennis = nextVal
                        "Volleyball" -> countDocument?.volleyball = nextVal
                        "Gym" -> countDocument?.gym = nextVal
                    }
                    counts[sports.size-i-1].text = "$nextVal users"
                }
                else {
                    switches[sports.size-i-1].isChecked = !isChecked
                    val snackbar: Snackbar = Snackbar
                        .make(window.decorView.findViewById(android.R.id.content), "You don't have an active internet connection. Please try again later", Snackbar.LENGTH_LONG)
                    snackbar.show()
                }
            }

            lookingForViewModel.getCountDocument()

            if (!connectivityAvailable) {
                val snackbar: Snackbar = Snackbar
                    .make(
                        window.decorView.findViewById(android.R.id.content),
                        "There was a problem connecting you to the internet. Some features might not be available",
                        Snackbar.LENGTH_LONG
                    )
                snackbar.show()
            }
        }
    }

    fun onGetCountDocument(it: CountDocument) {
        countDocument = it

        if (connectivityAvailable) {
            for (i in sports.indices.reversed()) {
                val cantUsers: TextView = counts[sports.size-i-1]

                when(sports[i].name)
                {
                    "Football" -> cantUsers.text = countDocument?.football.toString() + " users"
                    "Basketball" -> cantUsers.text = countDocument?.basketball.toString() + " users"
                    "Tennis" -> cantUsers.text = countDocument?.tennis.toString() + " users"
                    "Volleyball" -> cantUsers.text = countDocument?.volleyball.toString() + " users"
                    "Gym" -> cantUsers.text = countDocument?.gym.toString() + " users"
                }
            }
        }
    }

    private fun loadBackButton() {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = inflater.inflate(R.layout.back_button, null)
        parentFrameLayout = findViewById(R.id.frameLayout)
        parentFrameLayout!!.addView(view, 0)

        backButton = view.findViewById(R.id.imageButton)
        backButton?.setOnClickListener {
            finish()
        }
    }
}
