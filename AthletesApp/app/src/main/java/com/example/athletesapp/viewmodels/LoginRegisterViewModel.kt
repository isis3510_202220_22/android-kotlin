package com.example.athletesapp.viewmodels

import android.app.Application
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseUser
import androidx.annotation.RequiresApi
import android.os.Build
import com.example.athletesapp.models.repositories.AuthAppRepository
import com.example.athletesapp.models.repositories.UserRegisterRepository

class LoginRegisterViewModel(application: Application) : AndroidViewModel(application) {
    private val authAppRepository: AuthAppRepository
    val getRegisterPreferences: MutableLiveData<SharedPreferences>
    private val userRegisterRepository: UserRegisterRepository

    val userLiveData: MutableLiveData<FirebaseUser?>
    @RequiresApi(api = Build.VERSION_CODES.P)
    fun login(email: String?, password: String?) {
        authAppRepository.login(email, password)
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    fun register(email: String?, password: String?, name: String?, username: String?, favSport: String?) {
       authAppRepository.register(email,password,name, username, favSport)
    }

    fun removePreferences(){
        userRegisterRepository.removePreferences()
    }

    fun updatePreferences(email: String?,password: String?,name: String?, username: String?, favSport: Int){
        userRegisterRepository.updatePreferences(email,password,name,username,favSport)
    }

    init {
        authAppRepository = AuthAppRepository(application)
        userLiveData = authAppRepository.userLiveData
        userRegisterRepository = UserRegisterRepository(application)
        getRegisterPreferences = userRegisterRepository.registerPreferences
        userRegisterRepository.getRegistrationPreferences()
    }
}
