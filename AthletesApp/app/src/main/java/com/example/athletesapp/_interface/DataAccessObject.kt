package com.example.athletesapp._interface

interface DataAccessObject<T> {
    fun getAll()
    fun getOneById(id:String?)
    fun insertOne(entity: T)
    fun deleteOne(entity: T)
}