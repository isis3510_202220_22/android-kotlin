package com.example.athletesapp.views

import android.app.Application
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.example.athletesapp.models.repositories.MatchRepository
import java.util.*

class DatePickerFragment(application: Application) : DialogFragment(), DatePickerDialog.OnDateSetListener {

    private var matchRepository: MatchRepository = MatchRepository(application)


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the current date as the default date in the picker
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        // Create a new instance of DatePickerDialog and return it
        return DatePickerDialog(requireContext(), this, year, month, day)
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
        matchRepository.saveDateProgress(year,month+1,day)
    }
}