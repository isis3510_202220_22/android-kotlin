package com.example.athletesapp.views

import android.accounts.NetworkErrorException
import android.app.Application
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.format.DateFormat
import android.text.format.DateFormat.is24HourFormat
import android.view.View
import android.widget.Button
import android.widget.TimePicker
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import com.example.athletesapp.MainActivity
import com.example.athletesapp.R
import com.example.athletesapp.models.repositories.MatchRepository
import com.example.athletesapp.viewmodels.UserViewModel
import com.google.android.material.snackbar.Snackbar
import okhttp3.internal.wait
import java.io.IOException
import java.util.*
import kotlin.concurrent.thread

class TimePickerFragment (application: Application,view: View) : DialogFragment(), TimePickerDialog.OnTimeSetListener {

    private var matchRepository: MatchRepository = MatchRepository(application)
    private var userViewModel: UserViewModel = UserViewModel(application)
    private var viewAux = view
    private var app = application

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the current time as the default values for the picker
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)

        // Create a new instance of TimePickerDialog and return it
        return TimePickerDialog(activity, this, hour, minute, DateFormat.is24HourFormat(activity))
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        // Do something with the time chosen by the user
        try {
            matchRepository.saveTimeProgress(hourOfDay, minute)
            userViewModel.registerMatch()
            userViewModel.removePreferences()
            Snackbar.make(viewAux, "Match successfully registered", Snackbar.LENGTH_LONG).show()
            viewAux.findViewById<Button>(R.id.registerMatchButton).isEnabled = false
        } catch(e: IOException){
            Snackbar.make(viewAux, "There was an error registering your match. Try again later.", Snackbar.LENGTH_LONG).show()
            viewAux.findViewById<Button>(R.id.registerMatchButton).isEnabled = false
        }
        catch(e: NetworkErrorException)
        {
            Snackbar.make(viewAux, "No internet. Let's try it once you regain the connection", Snackbar.LENGTH_LONG).show()
        }
    }

}