package com.example.athletesapp.models.repositories

import android.app.Application
import com.example.athletesapp.models.Sport

class SportRepository(private val application: Application) {
    private var sports: ArrayList<Sport> = arrayListOf()

    fun getSports(): ArrayList<Sport> {
        if (sports.size == 0) {
            addSports()
        }
        return sports
    }

    fun getSportByName(name: String): Sport {
        if (sports.size == 0) {
            addSports()
        }

        for (i in sports) {
            if (i.name.lowercase() == name.lowercase()) {
                return i
            }
        }
        return sports[0]
    }

    private fun addSports() {
        sports.add(
            Sport("Football",
            "https://athletim.s3.amazonaws.com/ic_football_icon-playstore.png",
            "https://athletim.s3.amazonaws.com/ic_football_icon_yellow-playstore.png",
            "https://athletim.s3.amazonaws.com/ic_football_icon_black-playstore.png")
        )
        sports.add(
            Sport("Basketball",
            "https://athletim.s3.amazonaws.com/ic_basket2-playstore.png",
            "https://athletim.s3.amazonaws.com/ic_basket2_yellow-playstore.png",
            "https://athletim.s3.amazonaws.com/ic_basket2_black-playstore.png")
        )
        sports.add(
            Sport("Tennis",
            "https://athletim.s3.amazonaws.com/ic_tennis_icon-playstore.png",
            "https://athletim.s3.amazonaws.com/ic_tennis_icon_yellow-playstore.png",
            "https://athletim.s3.amazonaws.com/ic_tennis_icon_black-playstore.png")
        )
        sports.add(
            Sport("Volleyball",
            "https://athletim.s3.amazonaws.com/ic_volleyball_icon-playstore.png",
            "https://athletim.s3.amazonaws.com/ic_volleyball_icon_yellow-playstore.png",
            "https://athletim.s3.amazonaws.com/ic_volleyball_icon_black-playstore.png")
        )
        sports.add(
            Sport("Gym",
            "https://athletim.s3.amazonaws.com/ic_gym_icon-playstore.png",
            "https://athletim.s3.amazonaws.com/ic_gym_icon_yellow-playstore.png",
            "https://athletim.s3.amazonaws.com/ic_gym_icon_black-playstore.png")
        )
    }
}