package com.example.athletesapp.models.repositories

import android.util.Log
import android.util.SparseArray
import androidx.lifecycle.MutableLiveData
import com.example.athletesapp.models.Venue
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.Query

class VenueRepository() {
    private var dbVenues = FirebaseFirestore.getInstance().collection("venues")
    private val cache: SparseArray<DocumentSnapshot> = SparseArray<DocumentSnapshot>()

    private lateinit var nextQuery: Query

    val getListLiveData: MutableLiveData<List<Venue>> by lazy {
        MutableLiveData<List<Venue>>()
    }

    fun getVenuesPage(pageNum: Int, sport: String) {
        val document = cache[pageNum]
        if (document != null) {
            nextQuery =  dbVenues.whereEqualTo("sport",sport).orderBy("name", Query.Direction.DESCENDING).startAt(document).limit(PAGE_SIZE)
        }
        else if (pageNum == 0){
            nextQuery = dbVenues.whereEqualTo("sport",sport).orderBy("name", Query.Direction.DESCENDING).limit(PAGE_SIZE)
        }

        nextQuery.get().addOnSuccessListener { documentSnapshots ->
            val venues = ArrayList<Venue>()
            for (item in documentSnapshots.documents){
                val venue = Venue()
                venue.name = item.data!!["name"] as String?
                venue.address = item.data!!["address"] as String?
                venue.description = item.data!!["description"] as String?
                venue.img = item.data!!["img"] as String?
                venue.rating = item.data!!["rating"] as Number?
                venue.location = item.data!!["location"] as GeoPoint?
                venue.thumbnail = item.data!!["thumbnail"] as String?
                if (venue.thumbnail == null){
                    venue.thumbnail = venue.img
                }

                venues.add(venue)
            }

            if (document == null && !documentSnapshots.isEmpty){
                val firstVisible = documentSnapshots.documents[0]
                val lastVisible = documentSnapshots.documents[documentSnapshots.size() - 1]
                cache.put(pageNum,firstVisible)
                nextQuery = dbVenues.whereEqualTo("sport",sport).orderBy("name", Query.Direction.DESCENDING).startAfter(lastVisible).limit(PAGE_SIZE)
            }

            getListLiveData.postValue(venues)

        }.addOnFailureListener {
            Log.d("get", it.localizedMessage!!)
            getListLiveData.postValue(null)
        }
    }

    companion object {
        const val PAGE_SIZE: Long = 5
    }
}
