package com.example.athletesapp.models

import java.io.Serializable

data class User(
    var name: String? = null,
    var username: String? = null,
    var cantFriends: Number? = null,
    var cantMatches: Number? = null,
    var favSport: String? = null,
    var cantMatchesFavSport: Number? = null,
    var pictureLocation: String? = null,
    var email: String? = null,
): Serializable {
    fun toMap(): Map<String,Any?>{
        return mapOf(
            "name" to name,
            "username" to username,
            "cantFriends" to cantFriends,
            "cantMatches" to cantMatches,
            "favSport" to favSport,
            "cantMatchesFavSport" to cantMatchesFavSport,
            "pictureLocation" to pictureLocation,
            "email" to email,
        )
    }
}
