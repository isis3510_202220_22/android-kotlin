package com.example.athletesapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseUser
import androidx.annotation.RequiresApi
import android.os.Build
import com.example.athletesapp.models.repositories.AuthAppRepository

class AuthViewModel(application: Application) : AndroidViewModel(application) {
    private val authAppRepository: AuthAppRepository
    val userLiveData: MutableLiveData<FirebaseUser?>
    @RequiresApi(api = Build.VERSION_CODES.P)
    fun login(email: String?, password: String?) {
        authAppRepository.login(email, password)
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    fun register(email: String?, password: String?) {
        authAppRepository.register(email, password,null, null, null)
    }

    init {
        authAppRepository = AuthAppRepository(application)
        userLiveData = authAppRepository.userLiveData
    }
}
