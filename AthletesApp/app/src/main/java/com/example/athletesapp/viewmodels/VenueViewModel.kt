package com.example.athletesapp.viewmodels

import android.app.Application
import android.content.SharedPreferences
import android.service.autofill.FieldClassification
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.athletesapp.models.repositories.MatchRepository
import com.example.athletesapp.models.repositories.VenueRepository
import com.example.athletesapp.models.Venue

class VenueViewModel(application: Application): AndroidViewModel(application){
    private val repository = VenueRepository()
    private val matchRepository = MatchRepository(application)
    val progressEncounter: MutableLiveData<Boolean> = matchRepository.encounterRegistered

    fun getVenuesPage(pageNum: Int, sport: String){
        repository.getVenuesPage(pageNum,sport)
    }

    fun getLiveListData() : MutableLiveData<List<Venue>> {
        return repository.getListLiveData
    }

    fun saveUserProgress(userID: String?) {
        matchRepository.saveUserProgress(userID)
    }
}
