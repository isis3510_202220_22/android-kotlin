package com.example.athletesapp.models.repositories

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.athletesapp.models.CountDocument
import com.example.athletesapp.models.LookingFor
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore

class LookingForRepository(private val application: Application) {
    private var db = FirebaseFirestore.getInstance()
    private val collectionLookingFor:String = "lookingFor"
    var lastVisibleBySport: DocumentSnapshot? = null

    val getLookingForLiveData: MutableLiveData<LookingFor> by lazy {
        MutableLiveData<LookingFor>()
    }

    val getLookingForsBySportLiveData: MutableLiveData<ArrayList<LookingFor>> by lazy {
        MutableLiveData<ArrayList<LookingFor>>()
    }

    val getCountDocumentLiveData: MutableLiveData<CountDocument> by lazy {
        MutableLiveData<CountDocument>()
    }

    fun getOneById(id: String?) {
        db.collection(collectionLookingFor)
            .whereEqualTo("email", id)
            .get()
            .addOnSuccessListener { lookingFors ->
                val actualLookingFor = LookingFor()
                for (lookingFor in lookingFors) {
                    actualLookingFor.id = lookingFor.id
                    actualLookingFor.football = lookingFor.data["football"] as Boolean?
                    actualLookingFor.basketball = lookingFor.data["basketball"] as Boolean?
                    actualLookingFor.tennis = lookingFor.data["tennis"] as Boolean?
                    actualLookingFor.volleyball = lookingFor.data["volleyball"] as Boolean?
                    actualLookingFor.gym = lookingFor.data["gym"] as Boolean?
                    actualLookingFor.email = lookingFor.data["email"] as String?
                }
                getLookingForLiveData.postValue(actualLookingFor)
            }.addOnFailureListener{
                Log.d("get", it.localizedMessage!!)
            }
    }

    fun getBySport(sport: String, email: String) {
        db.collection(collectionLookingFor)
            .whereEqualTo(sport.lowercase(), true)
            .limit(500)
            .get()
            .addOnSuccessListener { lookingFors ->
                lastVisibleBySport = lookingFors.documents[lookingFors.size() - 1]

                val listLookingFor = arrayListOf<LookingFor>()
                for (lookingFor in lookingFors) {
                    if (lookingFor.data["email"]?.equals(email) == true) {
                        continue
                    }

                    val actualLookingFor = LookingFor()
                    actualLookingFor.id = lookingFor.id
                    actualLookingFor.email = lookingFor.data["email"] as String?
                    listLookingFor.add(actualLookingFor)
                }

                getLookingForsBySportLiveData.postValue(listLookingFor)
            }.addOnFailureListener{
                Log.d("get", it.localizedMessage!!)
            }
    }

    fun getCountDocument() {
        db.collection("sportsUsersCount")
            .get()
            .addOnSuccessListener { lookingFors ->
                val actualCountDocument = CountDocument()
                for (lookingFor in lookingFors) {
                    actualCountDocument.id = lookingFor.id
                    actualCountDocument.football = lookingFor.data["football"] as Number?
                    actualCountDocument.basketball = lookingFor.data["basketball"] as Number?
                    actualCountDocument.tennis = lookingFor.data["tennis"] as Number?
                    actualCountDocument.volleyball = lookingFor.data["volleyball"] as Number?
                    actualCountDocument.gym = lookingFor.data["gym"] as Number?
                }
                getCountDocumentLiveData.postValue(actualCountDocument)
            }.addOnFailureListener{
                Log.d("get", it.localizedMessage!!)
            }
    }
}
