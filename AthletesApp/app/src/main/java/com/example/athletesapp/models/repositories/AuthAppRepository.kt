package com.example.athletesapp.models.repositories

import android.app.Application
import android.content.ContentValues
import android.os.Build
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseUser
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.Exception

class AuthAppRepository(private val application: Application) {
    private val firebaseAuth: FirebaseAuth
    val userLiveData: MutableLiveData<FirebaseUser?>
    val loggedOutLiveData: MutableLiveData<Boolean>
    private val collectionUsers:String = "users"
    private val collectionLookingFor:String = "lookingFor"
    private var db = FirebaseFirestore.getInstance()

    @RequiresApi(Build.VERSION_CODES.P)
    fun login(email: String?, password: String?) {
        firebaseAuth.signInWithEmailAndPassword(email!!, password!!)
            .addOnCompleteListener(application.mainExecutor) { task ->
                if (task.isSuccessful) {
                    userLiveData.postValue(firebaseAuth.currentUser)
                } else {
                    Toast.makeText(
                        application.applicationContext,
                        "Login Failure: " + task.exception!!.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    fun register(email: String?, password: String?, name:String?, username: String?, favSport: String?) {
        firebaseAuth.createUserWithEmailAndPassword(email!!, password!!)
            .addOnCompleteListener(application.mainExecutor) { task ->
                if (task.isSuccessful) {
                    userLiveData.postValue(firebaseAuth.currentUser)
                    val user = hashMapOf(
                        "cantFriends" to 0,
                        "cantMatches" to 0,
                        "cantMatchesFavSport" to 0,
                        "email" to email,
                        "favSport" to favSport?.lowercase(),
                        "name" to name,
                        "pictureLocation" to "@mipmap/ic_user_icon",
                        "username" to "@$username",
                    )
                    db.collection(collectionUsers).add(user).addOnFailureListener { e -> Log.w (
                        ContentValues.TAG, "Error adding document",
                        e
                    )}

                    val lookingFor = hashMapOf(
                        "football" to false,
                        "basketball" to false,
                        "tennis" to false,
                        "volleyball" to false,
                        "gym" to false,
                        "email" to email
                    )

                    db.collection(collectionLookingFor).add(lookingFor).addOnFailureListener { e -> Log.w (
                        ContentValues.TAG, "Error adding document",
                        e
                    )}
                } else {
                    Toast.makeText(
                        application.applicationContext,
                        "Registration Failure: " + task.exception!!.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

    fun logOut() {
        firebaseAuth.signOut()
        loggedOutLiveData.postValue(true)
    }

    fun getActiveUserEmail(): String? {
        if (firebaseAuth.currentUser != null) {
            return firebaseAuth.currentUser!!.email
        }
        else {
            return null
        }
    }

    fun updatePassword(email: String, oldPassword: String, newPassword: String){
        firebaseAuth.currentUser?.reauthenticate(EmailAuthProvider.getCredential(email,oldPassword))
        Thread.sleep(1000)
        firebaseAuth.currentUser?.updatePassword(newPassword)
    }

    init {
        firebaseAuth = FirebaseAuth.getInstance()
        userLiveData = MutableLiveData()
        loggedOutLiveData = MutableLiveData()
        if (firebaseAuth.currentUser != null) {
            userLiveData.postValue(firebaseAuth.currentUser)
            loggedOutLiveData.postValue(false)
        }
    }
}
