package com.example.athletesapp.models.repositories

import android.accounts.NetworkErrorException
import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import com.example.athletesapp.utils.ConnectionProvider
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class MatchRepository(private val application: Application) {
    val sharedPref = application.getSharedPreferences("progress_tracker", Context.MODE_PRIVATE)
    private val collectionMatches:String = "matches"
    private var db = FirebaseFirestore.getInstance()

    val progressEncounter: MutableLiveData<SharedPreferences> by lazy {
        MutableLiveData<SharedPreferences>()
    }
    val encounterRegistered: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }


    fun saveSportProgress(sport: String?) {
        val sharedPref = application.getSharedPreferences("progress_tracker",Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString("sport", sport)
            apply()
        }
    }

    fun saveUserProgress(userID: String?) {
        val sharedPref = application.getSharedPreferences("progress_tracker",Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString("user", userID)
            apply()
        }
    }

    fun saveVenueProgress(venueName: String?) {
        val sharedPref = application.getSharedPreferences("progress_tracker",Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString("venue", venueName)
            apply()
        }
    }

    fun saveDateProgress(year: Int, month: Int, day: Int){
        val sharedPref = application.getSharedPreferences("progress_tracker", Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putInt("year", year)
            putInt("month", month)
            putInt("day", day)
            apply()
        }
    }

    fun saveTimeProgress(hour: Int, minute: Int){
        val sharedPref = application.getSharedPreferences("progress_tracker", Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putInt("hour", hour)
            putInt("minute", minute)
            apply()
        }
    }

    fun saveActualuser(userId: String?)
    {
        val sharedPref = application.getSharedPreferences("progress_tracker", Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString("email", userId)
            apply()
        }
    }

    fun publishProgress(){
        progressEncounter.postValue(sharedPref)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun registerMatch(){
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
        val sharedPref = application.getSharedPreferences("progress_tracker", Context.MODE_PRIVATE) ?: return
        val match = hashMapOf(
            "dateCreated" to LocalDateTime.now().format(formatter),
            "matchDate" to ""+sharedPref.getInt("year",0)+"-"+sharedPref.getInt("month",0)+"-"+sharedPref.getInt("day",0)+" "+sharedPref.getInt("hour",0)+":"+sharedPref.getInt("minute",0),
            "sport" to if(sharedPref.getString("sport",null)!=null){sharedPref.getString("sport",null)!=null} else{ throw IOException()
                return},
            "user1" to if(sharedPref.getString("email",null)!=null){sharedPref.getString("sport",null)!=null} else{ throw IOException()
                return},
            "user2" to if(sharedPref.getString("user",null)!=null){sharedPref.getString("sport",null)!=null} else{ throw IOException()
                return},
            "venue" to if(sharedPref.getString("venue",null)!=null){sharedPref.getString("sport",null)!=null} else{ throw IOException()
                return},
        )

        val connectionProvider:ConnectionProvider = ConnectionProvider(application.applicationContext)

        if(!connectionProvider.connectivityAvailable())
        {
            throw NetworkErrorException()
            return
        }

        db.collection(collectionMatches).add(match).addOnFailureListener { e -> Log.w (
            ContentValues.TAG, "Error adding document",
            e
        )
        throw IOException()
        }.addOnCompleteListener({
            encounterRegistered.postValue(true)
        })
    }



    fun removePreferences(){
        val sharedPref = application.getSharedPreferences("progress_tracker",Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            clear()
            apply()
        }
    }
}
