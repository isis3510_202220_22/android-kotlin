package com.example.athletesapp.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.athletesapp.databinding.ActivityAuthBinding

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
