package com.example.athletesapp.views

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.athletesapp.R
import com.example.athletesapp.viewmodels.LoginRegisterViewModel

class LoginFragment : Fragment() {
    private var emailEditText: EditText? = null
    private var passwordEditText: EditText? = null
    private var loginButton: Button? = null
    private var loginRegisterViewModel: LoginRegisterViewModel? = null
    private var registerLink: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginRegisterViewModel = ViewModelProviders.of(this).get(
            LoginRegisterViewModel::class.java
        )
        loginRegisterViewModel!!.userLiveData.observe(this) { firebaseUser ->
            if (firebaseUser != null) {
                Navigation.findNavController(requireView())
                    .navigate(R.id.action_loginRegisterFragment_to_loggedInFragment)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        emailEditText = view.findViewById(R.id.nameTextField)
        passwordEditText = view.findViewById(R.id.passwordTextField)
        loginButton = view.findViewById(R.id.signupButton)
        loginButton?.setOnClickListener(View.OnClickListener {
            val email = emailEditText?.getText().toString()
            val password = passwordEditText?.getText().toString()
            if (email.length > 0 && password.length > 0) {
                loginRegisterViewModel!!.login(email, password)
            } else {
                Toast.makeText(
                    context,
                    "Email Address and Password Must Be Entered",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

        registerLink = view.findViewById(R.id.signinLink)


        registerLink?.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(requireView())
                .navigate(R.id.action_FirstFragment_to_registerFragment)
        })

        return view
    }
}
