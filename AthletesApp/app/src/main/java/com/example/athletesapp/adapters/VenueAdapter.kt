package com.example.athletesapp.adapters

import android.content.Intent
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.athletesapp.R
import com.example.athletesapp.models.Venue
import com.example.athletesapp.utils.MyCache
import com.example.athletesapp.views.VenueDetailActivity
import com.example.athletesapp.views.VenuesListActivity
import kotlinx.coroutines.*
import java.net.URL
import kotlin.math.roundToInt

class VenueAdapter(var venueList: List<Venue>): RecyclerView.Adapter<VenueAdapter.VenueViewHolder>() {

    class VenueViewHolder(val view: View, val parent: ViewGroup): RecyclerView.ViewHolder(view){
        val nameTextView: TextView = view.findViewById(R.id.venue_name)
        val venuePictureFrame: FrameLayout = view.findViewById(R.id.imageFrame)
        val distanceTextView: TextView = view.findViewById(R.id.distance_text)
        val detailButton: ImageView = view.findViewById(R.id.detailButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.card_item, parent, false)

        return VenueViewHolder(view, parent)
    }

    override fun onBindViewHolder(holder: VenueViewHolder, position: Int) {
        val item = venueList[position]

        //getting pic from S3 bucket
        GlobalScope.launch(Dispatchers.IO) {
            getImage(item.thumbnail,holder)
        }

        holder.nameTextView.text = item.name.toString()
        var distText: String = try {
            "Distance: ${roundDist(item.distance)} km"
        } catch (e: Exception){
            ""
        }

        holder.distanceTextView.text = distText
        holder.detailButton.setOnClickListener {
            launchDetailView(it,item)
        }
    }

    override fun getItemCount(): Int {
        return venueList.size
    }

    private fun roundDist(dist: Double?): Double{
        return (dist!!*100.0).roundToInt() / 100.0
    }

    private suspend fun getImage(imgUrl: String?, holder: VenueViewHolder){
        val imgContainer: ConstraintLayout = LayoutInflater
            .from(holder.parent.context)
            .inflate(R.layout.venue_img, holder.parent, false) as ConstraintLayout

        val imgView = imgContainer.findViewById<ImageView>(R.id.venue_picture)

        var img = MyCache.instance.retrieveBitmapFromCache(imgUrl!!)
        // Img found in cache
        if (img != null){
            withContext(Dispatchers.Main){
                holder.venuePictureFrame.removeAllViews()
                imgView.setImageBitmap(img)
                holder.venuePictureFrame.addView(imgContainer)
            }
        }
        // Img not found in cache
        else {
            try {
                val newUrl = URL(imgUrl)
                img = BitmapFactory.decodeStream(newUrl.openConnection().getInputStream())
                MyCache.instance.saveBitmapToCache(imgUrl,img) //Saving img to LRU cache
                withContext(Dispatchers.Main){
                    holder.venuePictureFrame.removeAllViews()
                    imgView.setImageBitmap(img)
                    holder.venuePictureFrame.addView(imgContainer)
                }
            }
            catch (e: Exception){
                withContext(Dispatchers.Main){
                    holder.venuePictureFrame.removeAllViews()
                    imgView.setImageResource(R.drawable.broken_image)
                    holder.venuePictureFrame.addView(imgContainer)
                }
            }
        }
    }

    private fun launchDetailView(view:View, venue: Venue) {
        val intent = Intent(view.context,VenueDetailActivity::class.java)
        intent.putExtra("address",venue.address)
        intent.putExtra("description",venue.description)
        intent.putExtra("img",venue.img)
        intent.putExtra("latitude",venue.location?.latitude)
        intent.putExtra("longitude",venue.location?.longitude)
        intent.putExtra("name",venue.name)
        intent.putExtra("rating",venue.rating?.toDouble())
        view.context.startActivity(intent)
    }

}