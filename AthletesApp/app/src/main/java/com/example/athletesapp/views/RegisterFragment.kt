package com.example.athletesapp.views

import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.athletesapp.R
import com.example.athletesapp.databinding.FragmentRegisterBinding
import com.example.athletesapp.viewmodels.LoginRegisterViewModel


class RegisterFragment : Fragment() {
    private var loginRegisterViewModel: LoginRegisterViewModel? = null
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginRegisterViewModel = ViewModelProviders.of(this).get(
            LoginRegisterViewModel::class.java
        )
        loginRegisterViewModel!!.getRegisterPreferences.observe(this, Observer { preferences ->
            binding.emailTextField.setText(preferences.getString("email",null), TextView.BufferType.EDITABLE)
            binding.passwordTextField.setText(preferences.getString("password",null), TextView.BufferType.EDITABLE)
            binding.nameTextField.setText(preferences.getString("name",null), TextView.BufferType.EDITABLE)
            binding.usernameTextField.setText(preferences.getString("username",null), TextView.BufferType.EDITABLE)
            binding.spinner2.setSelection(preferences.getInt("favSport",0))
        })
    }

    override fun onStop() {

        super.onStop();
        loginRegisterViewModel!!.updatePreferences(binding.emailTextField.text.toString(),binding.passwordTextField.text.toString(),binding.nameTextField.text.toString(),binding.usernameTextField.text.toString(),binding.spinner2.selectedItemId.toInt())

    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View {

        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        val view = binding.root

        binding.signinLink.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(requireView())
                .navigate(R.id.action_registerFragment_to_loginRegisterFragment)
        })

        binding.signupButton.setOnClickListener(View.OnClickListener() {
            val email = binding.emailTextField.text.toString();
            val password = binding.passwordTextField.text.toString();
            val name =binding.nameTextField.text.toString();
            val username = binding.usernameTextField.text.toString();
            val favSport = binding.spinner2.selectedItem.toString()
            if (email.length > 0 && password.length > 6 && name.length > 0 && username.length>0 && !favSport.equals("Favorite sport") && isValidEmail(email)){
                try {
                    loginRegisterViewModel!!.register(email, password, name, username, favSport)
                    loginRegisterViewModel!!.removePreferences()
                    Thread.sleep(1000)
                    Navigation.findNavController(requireView())
                        .navigate(R.id.action_registerFragment_to_SecondFragment)
                }
                catch (e: Exception){
                }

            } else {
                if (favSport.equals("Favorite sport")){
                    Toast.makeText(getContext(), "Please choose a valid favorite sport and make sure all of the fields are filled out", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(!isValidEmail(email))
                    {
                        Toast.makeText(getContext(), "Please use a valid email address", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getContext(), "Email Address, Password (Must have 6 characters), Name and Username Must Be Entered", Toast.LENGTH_SHORT).show();
                    }

                }
            }
          })

        return view
    }
    fun isValidEmail(target: CharSequence?): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }
}
