package com.example.athletesapp.views

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.LruCache
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.athletesapp.R
import com.example.athletesapp.databinding.ActivityVenuesListBinding
import com.example.athletesapp.models.Venue
import com.example.athletesapp.adapters.VenueAdapter
import com.example.athletesapp.utils.ConnectionProvider
import com.example.athletesapp.viewmodels.VenueViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import java.lang.Exception
import java.util.ArrayList
import kotlin.math.acos
import kotlin.math.cos
import kotlin.math.sin

class VenuesListActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var nextPageButton: Button
    private lateinit var prevPageButton: Button
    private var buttonFrameLayout: FrameLayout? = null
    private var contentFrameLayout: FrameLayout? = null
    private var backButton: ImageButton? = null

    private lateinit var venuesList: ArrayList<Venue>
    private val venueViewModel: VenueViewModel by viewModels()
    private lateinit var venueAdapter: VenueAdapter
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private lateinit var selectedSport: String
    private lateinit var pagesCache: LruCache<Int,ArrayList<Venue>>
    private var pageNum = 0
    private lateinit var connectionProvider: ConnectionProvider

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityVenuesListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        recyclerView = binding.recyclerView
        contentFrameLayout = binding.contentFrame
        prevPageButton = binding.previousPageBut
        nextPageButton = binding.nextPageBut

        loadBackButton()

        connectionProvider = ConnectionProvider(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        useLocationService()

        selectedSport = intent.getStringExtra("sport").toString()

        pagesCache = LruCache(6)

        venueViewModel.saveUserProgress(intent.getStringExtra("user"))

        if (connectionProvider.connectivityAvailable()){
            prevPageButton.isEnabled = false
            prevPageButton.setOnClickListener {
                getPage(--pageNum)
                if (pageNum == 0) prevPageButton.isEnabled = false
            }
            nextPageButton.setOnClickListener {
                getPage(++pageNum)
                if (pageNum >= 0) prevPageButton.isEnabled = true
            }

            initViewModel()
        }
        else{
            loadFallbackScreen()
            Toast.makeText(this, "No connection", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initViewModel() {
        venueViewModel.getLiveListData().observe(this) {
            onGetList(it)
        }
    }

    private fun getPage (pageNum: Int) {
        var page = pagesCache.get(pageNum)
        if (page != null) {
            onGetList(page)
        }
        else {
            if (connectionProvider.connectivityAvailable()) {
                venueViewModel.getVenuesPage(pageNum,selectedSport)
            } else {
                loadFallbackScreen()
            }
        }
    }

    private fun onGetList(it: List<Venue>) {
        if (it.size < 5) nextPageButton.isEnabled = false
        else if (it.size == 5) nextPageButton.isEnabled = true

        if (latitude == 0.0 || longitude == 0.0){
            Toast.makeText(this, "Location not available", Toast.LENGTH_SHORT).show()
            venuesList = ArrayList()
            venuesList.addAll(it)
        }
        else{
            setVenuesList(it)
        }

        venueAdapter = VenueAdapter(venuesList)
        recyclerView.adapter = venueAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        prevPageButton.isEnabled = pageNum != 0
        venueAdapter.notifyDataSetChanged()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun useLocationService() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            val locationPermissionRequest = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                when {
                    permissions.getOrDefault(android.Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
                            if (location == null){
                                getLocationFromPreferences()
                            }
                            else{
                                latitude = location.latitude
                                longitude = location.longitude
                                saveLocationToPreferences(latitude.toString(),longitude.toString())
                            }
                            venueViewModel.getVenuesPage(pageNum,selectedSport)
                        }
                    }
                    permissions.getOrDefault(android.Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {
                        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
                            if (location == null){
                                getLocationFromPreferences()
                            }
                            else{
                                latitude = location.latitude
                                longitude = location.longitude
                                saveLocationToPreferences(latitude.toString(),longitude.toString())
                            }
                            venueViewModel.getVenuesPage(pageNum,selectedSport)
                        }
                    }
                    else -> {
                        getLocationFromPreferences()
                        venueViewModel.getVenuesPage(pageNum,selectedSport)
                    }
                }
            }
            locationPermissionRequest.launch(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION,android.Manifest.permission.ACCESS_COARSE_LOCATION))
            return
        }

        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            if (location == null){
                getLocationFromPreferences()
            }
            else{
                latitude = location.latitude
                longitude = location.longitude
                saveLocationToPreferences(latitude.toString(),longitude.toString())
            }
            venueViewModel.getVenuesPage(pageNum,selectedSport)
        }
    }

    private fun getLocationFromPreferences() {
        val sharedPref = this?.getPreferences(Context.MODE_PRIVATE) ?: return
        val lastLatitude = sharedPref.getString(SAVED_LATITUDE_KEY, "")
        val lastLongitude = sharedPref.getString(SAVED_LONGITUDE_KEY, "")
        try {
            latitude = lastLatitude!!.toDouble()
            longitude = lastLongitude!!.toDouble()
            Toast.makeText(this, "Using approximate location. Results may not be accurate.", Toast.LENGTH_LONG).show()
        }
        catch (e: Exception) {
            latitude = 0.0
            longitude = 0.0
        }
    }

    private fun saveLocationToPreferences(latitude: String, longitude: String) {
        val sharedPref = this?.getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(SAVED_LATITUDE_KEY,latitude)
            putString(SAVED_LONGITUDE_KEY,longitude)
            apply()
        }
    }

    private fun setVenuesList(it: List<Venue>){
        for (venue in it){
            var dist = distanceInKm(latitude, longitude, venue.location!!.latitude, venue.location!!.longitude)
            venue.distance = dist
        }
        venuesList = ArrayList()
        venuesList.addAll(it.sortedWith(compareBy { it.distance }))
    }

    private fun distanceInKm(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val theta = lon1 - lon2
        var dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(theta))
        dist = acos(dist)
        dist = rad2deg(dist)
        dist *= 60 * 1.1515
        dist *= 1.609344
        return dist
    }

    private fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    private fun rad2deg(rad: Double): Double {
        return rad * 180.0 / Math.PI
    }

    private fun loadBackButton() {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = inflater.inflate(R.layout.back_button, null)
        buttonFrameLayout = findViewById(R.id.backButtonFrame)
        buttonFrameLayout!!.addView(view, 0)

        backButton = view.findViewById(R.id.imageButton)
        backButton?.setOnClickListener(View.OnClickListener {
            finish()
        })
    }

    private fun loadFallbackScreen() {
        contentFrameLayout!!.removeView(recyclerView)
        prevPageButton.visibility = Button.INVISIBLE
        nextPageButton.visibility = Button.INVISIBLE
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = inflater.inflate(R.layout.connection_error, null)
        contentFrameLayout = findViewById(R.id.contentFrame)
        contentFrameLayout!!.addView(view, 0)
    }

    companion object {
        const val SAVED_LATITUDE_KEY: String = "LATITUDE"
        const val SAVED_LONGITUDE_KEY: String = "LONGITUDE"
    }
}
